package Controller;

import Model.Client;
import Model.Queue;
import View.View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;

public class Main {

    public static void main(String args[]){

        View view = new View();
        view.mainframe.setVisible(true);

        view.buttons[0].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                Simulation sim =  new Simulation(view);
                //sim.randomClientGenerator();
                //sim.queueCreation();
                Thread t = new Thread(sim);
                t.start();
            }
        });




    }
}
