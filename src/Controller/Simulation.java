package Controller;

import Model.Client;
import Model.Queue;
import View.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Simulation implements Runnable{
    View v = new View();
    public int nrOfQueues = 3;
    public int maxNrOfClients = 20;
    public int simulationInterval = 100;
    public Queue queueList[] = new Queue[nrOfQueues];
    public List<Client> clientList = new ArrayList<>();

    //public int minArrivalTime = Integer.parseInt(v.textFields[0].getText());
    //public int maxArrivalTime = Integer.parseInt(v.textFields[1].getText());
    //public int minServiceTime = Integer.parseInt(v.textFields[2].getText());
    //public int maxServiceTime = Integer.parseInt(v.textFields[3].getText());


    public void randomClientGenerator(){
        int at=1;
        for(int i=0;i<maxNrOfClients;i++){
            int serviceT = ThreadLocalRandom.current().nextInt(Integer.parseInt(v.textFields[2].getText()),Integer.parseInt(v.textFields[3].getText())+1);
            int arrivalTIntervalRandom = ThreadLocalRandom.current().nextInt(Integer.parseInt(v.textFields[0].getText()),Integer.parseInt(v.textFields[1].getText()));
            at = at+arrivalTIntervalRandom; //arrival time; it is added with the previous time and a random nr in the range given by the user
           //verify, if it is bigger than the simulation time, perform the following op
            if(at>=simulationInterval){
                at = at%simulationInterval+1;
            }

            Client newclient = new Client(serviceT,at);
            System.out.println("New client CREATED: Arrival: "+newclient.arrivalTime+" Service: "+newclient.serviceTime);
            this.clientList.add(newclient);
        }
        Collections.sort(clientList, new Comparator<Client>() {
            @Override
            public int compare(Client o1, Client o2) {
                return o1.getArrivalTime()-o2.getArrivalTime();
            }
        });
        System.out.println("Sorted client list: ");
        int j=1;
        for(Client c :clientList){
            System.out.println("Client "+j+" Arrival: "+c.arrivalTime+" Service: "+c.serviceTime);
            j++;
        }
    }

    public void queueCreation(){
        for(int i=0;i<nrOfQueues;i++){
            queueList[i] = new Queue(i+1);
            Thread thread = new Thread(queueList[i]);
            thread.start();
            System.out.println("Queue "+queueList[i].getQueue_id()+" created");
        }
    }

    public void clientManagement(Client newclient){
        int minWaitT=simulationInterval; //the biggest possible number - the simulation duration
        for(Queue q: queueList){
            if(q.getWaitingTime().get()< minWaitT){
                minWaitT = q.getWaitingTime().get();
            }
        }
        //System.out.println("Min waiting time: "+minWaitT);
        for(Queue queue: queueList){
            if(queue.getWaitingTime().get()==minWaitT){
                System.out.println("Min waiting time: "+minWaitT+" Queue "+queue.getQueue_id());
                System.out.println("Queue "+queue.getQueue_id()+": NEW client Arrival "+newclient.getArrivalTime()+" Service "+newclient.getServiceTime());
                queue.addClient(newclient);
                break;
                //System.out.println("Queue "+queue.getQueue_id()+"; NEW Client: Arrived: "+newclient.getArrivalTime()+"; Service: "+newclient.getServiceTime());
            }
        }

    }

    public Simulation(View view){this.v = view;}
    public Simulation(){}

    @Override
    public void run() {
        //int clock=0;
        queueCreation();
        randomClientGenerator();

        for(int clock=1;clock<=simulationInterval;clock++) {
            try {
                System.out.println("Time: " + clock + "/" + simulationInterval);
                for (Client cl : clientList) {
                    if (cl.arrivalTime == clock) {
                        //System.out.println("Client arrived: "+cl.arrivalTime+"  "+cl.serviceTime);
                        clientManagement(cl);
                    }
                }
                v.showSimulation(queueList);
                Thread.sleep(1000);
            }
            catch (Exception e){}
        }
    }
}
