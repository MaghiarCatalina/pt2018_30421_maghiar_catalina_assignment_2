package Model;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Queue implements Runnable{
    private static int queueCapacity = 20;
    public BlockingQueue<Client> clientBlockingQueue;
    public AtomicInteger waitingTime;
    public int queue_id;
    public Client servedClient;

    public Queue(int i ){
        queue_id=i;
        clientBlockingQueue = new ArrayBlockingQueue<Client>(queueCapacity);
        waitingTime = new AtomicInteger(0); // is the sum of all serviceTime needed by all the clients in a queue; initial is 0
    }

    public void addClient(Client newClient){
        clientBlockingQueue.add(newClient);
        waitingTime.addAndGet(newClient.serviceTime); //automatically adds to the current value
        //System.out.println("Queue "+getQueue_id()+": NEW client: Arrival "+ newClient.arrivalTime+"; Service  "+newClient.serviceTime+"; Waiting: "+getWaitingTime());
    }


    @Override
    public void run() {
        while(true) {
            try {
                // take() waits for an element to become available if none
                servedClient = clientBlockingQueue.take(); //primul de la coada merge la casa, deci este scos din coada
                //timpul de asteptare la coada scade cu serviceTime-ul clientului curent
                // thread-ul ia sleep pe perioada de service a clientului
                System.out.println("Queue "+getQueue_id()+ ": SERVING client Arrived: "+servedClient.arrivalTime+" Service: "+servedClient.serviceTime);
                Thread.sleep(servedClient.serviceTime * 1000);
                waitingTime.addAndGet((-1) * servedClient.serviceTime);
                System.out.println("Queue "+getQueue_id()+": DONE  Arrived: "+servedClient.arrivalTime+" Service: "+servedClient.serviceTime);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public AtomicInteger getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(AtomicInteger waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getQueue_id() {
        return queue_id;
    }

    public void setQueue_id(int queue_id) {
        this.queue_id = queue_id;
    }

    @Override
    public String toString() {
        String ret = queue_id+"";
        return ret;
    }
}
