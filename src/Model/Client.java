package Model;

public class Client {
    public int serviceTime;
    public int arrivalTime;
    int clientID;
    public Client(){}
    public Client(int serviceTime,int arrivalTime){
        this.serviceTime = serviceTime;
        this.arrivalTime = arrivalTime;
        clientID= (int)Math.random();
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public int getServiceTime() {
        return serviceTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public void setServiceTime(int serviceTime) {
        this.serviceTime = serviceTime;
    }
}
