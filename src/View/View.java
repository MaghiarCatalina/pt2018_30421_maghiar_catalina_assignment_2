package View;

import Model.Client;
import Model.Queue;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class View {
    public JFrame mainframe = new JFrame();
    public JPanel panel = new JPanel();
    public JTextField textFields[] = new JTextField[10];
    public JButton buttons[] = new JButton[5];
    public JLabel labels[] = new JLabel[10];
    public Label qLabel[] = new Label[10];
    public Label cLabel[] = new Label[50];

    public View(){
        mainframe.setSize(650,500);
        mainframe.setTitle("Queue Simulation");
        mainframe.add(panel);
        panel.setLayout(null);
        panel.setBackground(Color.gray);
        labels[0] = new JLabel("Min/Max arrival time: ");
        labels[0].setBounds(5,5,130,30);
        textFields[0] = new JTextField();
        textFields[1] = new JTextField();
        textFields[0].setBounds(140,5,30,25);
        textFields[1].setBounds(175,5,30,25);
        labels[1] = new JLabel("Min/Max service time: ");
        labels[1].setBounds(5,40,130,30);
        textFields[2] = new JTextField();
        textFields[3] = new JTextField();
        textFields[2].setBounds(140,40,30,25);
        textFields[3].setBounds(175,40,30,25);
        buttons[0] = new JButton("Start");
       // buttons[1] = new JButton("Exit");
        buttons[0].setBounds(5,90,70,30);
        buttons[0].setBackground(Color.green);
        /*
        buttons[1].setBounds(80,90,70,30);
        buttons[1].setBackground(Color.lightGray);

        buttons[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        */
        panel.add(labels[0]);
        panel.add(labels[1]);
        panel.add(textFields[0]);
        panel.add(textFields[1]);
        panel.add(textFields[2]);
        panel.add(textFields[3]);
        panel.add(buttons[0]);
        //panel.add(buttons[1]);

    }

    public void showSimulation(Queue qList[]){
        panel.removeAll();
        panel.revalidate();

        buttons[1] = new JButton("Exit");
        buttons[1].setBounds(80,90,70,30);
        buttons[1].setBackground(Color.lightGray);

        buttons[1].addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        panel.add(buttons[1]);

        for (int i=0;i<qList.length;i++){
            qLabel[i] = new Label("Queue "+qList[i].toString());
            qLabel[i].setAlignment(Label.CENTER);
            qLabel[i].setBounds(30+180*i,150,150,25);
            qLabel[i].setBackground(Color.darkGray);
            qLabel[i].setForeground(Color.white);
            panel.add(qLabel[i]);

            qLabel[i+3] = new Label("Current Client");
            qLabel[i+3].setAlignment(Label.CENTER);
            qLabel[i+3].setBounds(30+180*i,175,150,25);
            qLabel[i+3].setBackground(Color.cyan);
            panel.add(qLabel[i+3]);

            if(qList[i].servedClient!=null){
                qLabel[i+6] = new Label("Arrival "+qList[i].servedClient.arrivalTime+" Service "+qList[i].servedClient.serviceTime);
                qLabel[i+6].setAlignment(Label.CENTER);
                qLabel[i+6].setBounds(30+180*i,200,150,25);
                qLabel[i+6].setBackground(Color.cyan);
                panel.add(qLabel[i+6]);
            }

            int j=0;
            for(Client c: qList[i].clientBlockingQueue){

                cLabel[j] = new Label("Arrival "+c.arrivalTime+" Service "+c.serviceTime);
                cLabel[j].setAlignment(Label.CENTER);
                cLabel[j].setBackground(Color.lightGray);
                cLabel[j].setBounds(30+180*i,225+j*25,150,25);
                panel.add(cLabel[j]);
                j++;
            }
        }
        panel.repaint();
        panel.revalidate();

    }

}
